﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trakomatic_Quiz
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnQuestion1_Click(object sender, RoutedEventArgs e) // evaluation of expression
        {
            if(txtExpression.Text!="")
            {
                double result = Convert.ToDouble(new DataTable().Compute(txtExpression.Text, null));
                txtResultQues1.Text = result.ToString();
            }  
        }

        private void btnQuestion2_Click(object sender, RoutedEventArgs e) // number swapping
        {
            if (txtFirstNum.Text != "" && txtSecondNum.Text != "")
            {
                int n;
                if (int.TryParse(txtFirstNum.Text, out n) == true && int.TryParse(txtSecondNum.Text, out n) == true)
                {
                    int firstNum = int.Parse(txtFirstNum.Text), secondNum = int.Parse(txtSecondNum.Text);

                    firstNum = firstNum + secondNum;
                    secondNum = firstNum - secondNum;
                    firstNum = firstNum - secondNum;
                    txtFirstNum.Text = firstNum.ToString();
                    txtSecondNum.Text = secondNum.ToString();
                }
                else
                    MessageBox.Show("Enter integer number only");   
            }
            MessageBox.Show("enter two numbers to swap");
        }

        struct Employee
        {
            public string Name;
            public DOB dob;
        }

        struct DOB
        {
            public int date;
            public int month;
            public int year;
        }

        private void btnQuestion3_Click(object sender, RoutedEventArgs e) // nested struct
        {
            int total = 2; // for 2 employees
            Employee[] emp = new Employee[total];

            emp[0].Name = "Lalit";
            emp[0].dob.date = 24;
            emp[0].dob.month = 7;
            emp[0].dob.year = 1989;

            emp[1].Name = "Amit";
            emp[1].dob.date = 26;
            emp[1].dob.month = 10;
            emp[1].dob.year = 1987;

            MessageBox.Show("Name : " + emp[0].Name + "\nDate : " + emp[0].dob.date + "\nMonth : " + emp[0].dob.month + "\nYear : " + emp[0].dob.year + "\n\nName : " + emp[1].Name + "\nDate : " + emp[1].dob.date + "\nMonth : " + emp[1].dob.month + "\nYear : " + emp[1].dob.year);
        }     

        private void btnQuestion4_Click(object sender, RoutedEventArgs e) // Numbers and squares of each element in array
        {
            if (txtArray.Text != "")
            {
                string[] Numbers = txtArray.Text.Split(',');
                int n;
                string result = "";
                for (int i = 0; i < Numbers.Length; i++)
                {
                    if (int.TryParse(Numbers[i].Trim(), out n))
                    {
                        result += "{Number = " + Numbers[i].ToString() + ", SqrNo = " + int.Parse(Numbers[i].Trim()) * int.Parse(Numbers[i].Trim()) + "}\n";
                    }
                }
                MessageBox.Show(result);
            }
            else
                MessageBox.Show("Enter numbers seperated by comma(,)");
        }

        private void btnQuestion5_Click(object sender, RoutedEventArgs e) // get the character and frequency of each character of an string
        {
            if (txtCharString.Text != "")
            {
                int[] ch = new int[(int)char.MaxValue];
                foreach (char t in txtCharString.Text)
                {
                    ch[(int)t]++;
                }

                string result = "";
                for (int i = 0; i < (int)char.MaxValue; i++)
                {
                    if (ch[i] > 0 && char.IsLetterOrDigit((char)i))
                    {
                        result += "Character " + (char)i + ": " + ch[i] + " times\n";
                    }
                }
                MessageBox.Show(result);
            }
            else
                MessageBox.Show("Enter string to get frequency");
        }

        private void btnQuestion6_Click(object sender, RoutedEventArgs e) // number and multiplication of number and frequency
        {
            if (txtNumArray.Text != "")
            {
                List<int> numArr = new List<int>();
                string[] arr = txtNumArray.Text.Split(',');
                for (int i = 0; i < arr.Length; i++)
                    numArr.Add(int.Parse(arr[i].ToString()));
                
                string result = "Number | Number * Frequency | Frequency\n";
                foreach (var grp in numArr.GroupBy(i => i))
                {
                    result += grp.Key + " | " + grp.Key * grp.Count() + " | " + grp.Count() + "\n";
                }
                MessageBox.Show(result);
            }
            else
                MessageBox.Show("Enter numbers seperated by comma(,)");
        }

        private void btnCreateFile_Click(object sender, RoutedEventArgs e) // text file creation, editing and reading
        {
            System.IO.Directory.CreateDirectory(@"C:\Trakomatic_Test");
            string path = @"C:\Trakomatic_Test\mytest.txt";

            if (txtText.Text != "")
            {
                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                    using (TextWriter tw = new StreamWriter(path))
                    {
                        tw.WriteLine(txtText.Text);
                    }
                }
                else if (File.Exists(path))
                {
                    using (StreamWriter w = File.AppendText(path))
                    {
                        w.WriteLine(txtText.Text);
                        w.Close();
                    }
                }
                MessageBox.Show(System.IO.File.ReadAllText(path));
            }
            else
                MessageBox.Show("Enter the text");        
        }

        private void btnQuestion8_Click(object sender, RoutedEventArgs e) // converting decimal to binary using recursion
        {
            int n;
            if (txtDecimalNum.Text != "")
            {
                if (int.TryParse(txtDecimalNum.Text, out n))
                    txtBinaryNum.Text = ConversionToBinary(int.Parse(txtDecimalNum.Text)).ToString();
            }
            else
                MessageBox.Show("Enter decimal number to convert");
        }

        public int ConversionToBinary(int num)
        {
            int binary;
            if (num != 0)
            {
                binary = (num % 2) + 10 * ConversionToBinary(num / 2);  
                return binary;
            }
            else
                return 0; 
        }
    }
}
